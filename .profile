# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$HOME/.local/bin:$PATH:$HOME/.rvm/bin:$HOME/bin:$HOME/bin/wm:$HOME/.gem/ruby/2.7.0/bin"
export MOZ_USE_XINPUT2=1
export VISUAL=vim;
export EDITOR=vim;

# source ~/.upwork.aws.sh
