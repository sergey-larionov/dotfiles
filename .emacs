(require 'package)
(setenv "PATH" (concat (getenv "PATH") ":/home/larionov/.nvm/versions/node/v12.18.3/bin"))
(setq exec-path (append exec-path '("/home/larionov/.nvm/versions/node/v12.18.3/bin")))

(setenv "PATH" (concat (getenv "PATH") ":/home/larionov/.local/bin/zig"))
(setq exec-path (append exec-path '("/home/larionov/.local/bin/zig")))
(add-to-list 'load-path "~/elisp/")
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
		    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  (add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
)
(package-initialize)


(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)
(straight-use-package '(apheleia :host github :repo "raxod502/apheleia"))

(apheleia-global-mode +1)
;; Bootstrap `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))



(defvar my-packages
  '(
    ;; add-node-modules-path
    ;; blacken
    cider
    clojure-mode
    clojure-mode-extra-font-locking
    ;; color-theme-sanityinc-tomorrow
    company
    company-jedi
    company-quickhelp
    dockerfile-mode
    ;; dumb-jump
    editorconfig
    elpy
    emojify
    ;; exec-path-from-shell
    flycheck
    flycheck-clj-kondo
    ;; ido-completing-read+
    js2-mode
    json-mode
    ;; magit
    ;; markdown-mode
    ;; move-text
    ;; multiple-cursors
    ;; neotree
    ;; pandoc-mode
    paredit
    ;; pipenv
    ;; projectile
    ;; pyenv-mode-auto
    rainbow-delimiters
    smex
    super-save
    ;; tagedit
    ;; tide
    ;; web-mode
    which-key
    yaml-mode))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

(global-unset-key (kbd "C-z"))

(global-set-key (kbd "C-z C-z") 'my-suspend-frame)

(defun my-suspend-frame ()
  "In a GUI environment, do nothing; otherwise `suspend-frame'."
  (interactive)
  (if (display-graphic-p)
      (message "suspend-frame disabled for graphical displays.")
    (suspend-frame)))
;;
;;; GNU emacs customization file
;;;
;; The delay caused by set-default-font can be worked around by adding the following
;; at the beginning of your .emacs file:
(modify-frame-parameters nil '((wait-for-wm . nil)))
(add-hook 'before-save-hook 'delete-trailing-whitespace)
(tool-bar-mode -1)
(menu-bar-mode -1)  ;; shows full-screen button for mac port
(setq-default
 blink-cursor-delay 0.5
 blink-cursor-interval 1
 use-file-dialog nil
 use-dialog-box nil
 inhibit-startup-screen t
 inhibit-startup-echo-area-message t
 truncate-lines t
 truncate-partial-width-windows nil
 visible-bell 1
 transient-mark-mode t   ;; highlight the active region when mark is active
 show-trailing-whitespace t ;; don't show trailing whitespace globally
 blink-matching-paren t
 initial-frame-alist '((left-fringe . 1) (right-fringe . 1) (scroll-bar-width . 0) (vertical-scroll-bars . nil))
 default-frame-alist '((left-fringe . 1) (right-fringe . 1) (scroll-bar-width . 0) (vertical-scroll-bars . nil))
 scroll-bar-width 0
 default-frame-scroll-bars nil)
(global-display-line-numbers-mode 1)

(defun mode-line-bell ()
  (let ((orig (face-attribute 'mode-line :background)))
    (set-face-attribute 'mode-line nil :background "red")
    (sit-for 0 70)
    (set-face-attribute 'mode-line nil :background orig)))

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files

(setq backup-directory-alist '(("" . "~/.emacs.d/backup")))

(setq
 visible-bell nil
 ring-bell-function 'mode-line-bell)

(setq create-lockfiles nil)
(use-package "monokai-theme" :ensure t )
(load-theme 'monokai t)

(use-package "lua-mode" :ensure t )

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

(defconst inhibit-startup-message t)

(defun mydired-sort ()
  "Sort dired listings with directories first."
  (save-excursion
    (let (buffer-read-only)
      (forward-line 2) ;; beyond dir. header
      (sort-regexp-fields t "^.*$" "[ ]*." (point) (point-max)))
    (set-buffer-modified-p nil)))

(defadvice dired-readin
  (after dired-after-updating-hook first () activate)
  "Sort dired listings with directories first before adding marks."
  (mydired-sort))


(require 'saveplace)
(require 'dired+)
(require 'bookmark+)
(require 'highlight)
(require 'dired-arrow-keys)
(dired-arrow-keys-install)
(global-set-key (kbd "M-s") 'dired-jump)
(toggle-diredp-find-file-reuse-dir 1)

;; delete current map of ESC-[
(global-unset-key "\e[")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(Buffer-menu-sort-column 1)
 '(apheleia-formatters
   '((black "black" "-")
     (brittany "brittany" file)
     (prettier npx "/home/larionov/projects/vue-components/node_modules/.bin/prettier" "--stdin-filepath" filepath)
     (gofmt "gofmt")
     (zigfmt "zig" "fmt" file)
     (ocamlformat "ocamlformat" file)
     (terraform "terraform" "fmt" "-")))
 '(apheleia-mode-alist
   '((css-mode . prettier)
     (go-mode . gofmt)
     (zig-mode . zigfmt)
     (js-mode . prettier)
     (js3-mode . prettier)
     (json-mode . prettier)
     (html-mode . prettier)
     (python-mode . black)
     (sass-mode . prettier)
     (typescript-mode . prettier)
     (web-mode . prettier)
     (yaml-mode . prettier)
     (terraform-mode . terraform)
     (tuareg-mode . ocamlformat)
     (haskell-mode . brittany)
     (gfm-mode . prettier)))
 '(bmkp-last-as-first-bookmark-file "/home/larionov/.emacs.d/bookmarks")
 '(dired-listing-switches "-alh")
 '(diredp-wrap-around-flag nil)
 '(monokai-background "#151515")
 '(monokai-highlight "#19483E")
 '(package-selected-packages
   '(zig-mode clj-refactor js2-mode web-mode markdown-mode counsel ibuffer-vc hydra ivy smex flx-ido flx magit helm-ag helm-ls-git helm helm-projectile editorconfig cider clojure-mode lua-mode monokai-theme use-package))
 '(projectile-completion-system 'ivy)
 '(projectile-globally-ignored-directories
   '(".idea" ".ensime_cache" ".eunit" ".git" ".hg" ".fslckout" "_FOSSIL_" ".bzr" "_darcs" ".tox" ".svn" ".stack-work" "*node_modules"))
 '(safe-local-variable-values
   '((ssh-deploy-root-remote . "/ssh:root@172.18.22.217#22080:/srv/www/vendor/odesk/ui-components-bundle/")
     (ssh-deploy-root-local . "/home/larionov/projects/ui-components-bundle/")
     (ssh-deploy-script lambda nil
                        (let
                            ((default-directory ssh-deploy-root-remote))
                          (shell-command "bash compile.sh")))
     (ssh-deploy-on-explicit-save . 1)
     (ssh-deploy-async . 1)
     (ssh-deploy-root-remote . "/ssh:sandbox5:user-interface-binder/vendor/upwork/nuxt-api-bundle/")
     (ssh-deploy-root-local . "/home/larionov/projects/nuxt-api-bundle/"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'dired-find-alternate-file 'disabled nil)

(defalias 'yes-or-no-p 'y-or-n-p)
(show-paren-mode 1)

;; auto-complete compatibility
;; (defun my-company-visible-and-explicit-action-p ()
;;   (and (company-tooltip-visible-p)
;;        (company-explicit-action-p)))

;; (defun company-ac-setup ()
;;   "Sets up `company-mode' to behave similarly to `auto-complete-mode'."
;;   (setq company-require-match nil)
;;   (setq company-auto-complete #'my-company-visible-and-explicit-action-p)
;;   (setq company-frontends '(company-echo-metadata-frontend
;;                             company-pseudo-tooltip-unless-just-one-frontend-with-delay
;;                             company-preview-frontend))
;;   (define-key company-active-map [tab]
;;     'company-select-next-if-tooltip-visible-or-complete-selection)
;;   (define-key company-active-map (kbd "TAB")
;;     'company-select-next-if-tooltip-visible-or-complete-selection))

;; (use-package company :ensure t :defer 20
;;   ;; This is not perfect yet. It completes too quickly outside programming modes, but while programming it is just right.
;;   :custom
;;   (company-idle-delay 0.1)
;;   (global-company-mode t)
;;   (debug-on-error nil) ;; otherwise this throws lots of errors on completion errors
;;   :config
;;   (define-key company-active-map (kbd "TAB") 'company-complete-selection)
;;   (define-key company-active-map (kbd "<tab>") 'company-complete-selection)
;;   ;;(define-key company-active-map [return] 'company-complete-selection)
;;   ;;(define-key company-active-map (kbd "RET") 'company-complete-selection)

;;   (define-key company-active-map (kbd "<return>") nil)
;;   (define-key company-active-map (kbd "RET") nil)
;;   (define-key company-active-map (kbd "C-SPC") 'company-complete-selection)
;;   (company-ac-setup)
;;   (add-hook 'web-mode-hook (lambda () (company-mode t)))
;;   (add-hook 'js-mode-hook (lambda () (company-mode t)))
;;   (add-hook 'js2-mode-hook (lambda () (company-mode)))
;;   )



;; (use-package company-quickhelp :ensure t :defer 30
;;   :config
;;   (company-quickhelp-mode t))


;; (add-hook 'web-mode-hook (lambda () (prettier-mode t)))
;; (add-hook 'js-mode-hook (lambda () (prettier-mode t)))
;; (add-hook 'js2-mode-hook (lambda () (prettier-mode t)))


(use-package go-mode :ensure t :defer 30)
(add-hook 'before-save-hook #'gofmt-before-save)
;; SCHEMA
(use-package clojure-mode :ensure t )
(use-package cider

  :ensure t
  :pin melpa-stable)

(use-package clj-refactor
  :ensure t)

(autoload 'zap-up-to-char "misc"
  "Kill up to, but not including ARGth occurrence of CHAR." t)


;; VUE
(use-package web-mode :ensure t)
(use-package js2-mode :ensure t)
(use-package editorconfig
  :ensure t
  :config )
;; (use-package vue-mode
;;   :ensure t
;;   :config
;;   ;; 0, 1, or 2, representing (respectively) none, low, and high coloring
;;   (setq mmm-submode-decoration-level 0))

(setq-default tab-width 2)
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.svelte\\'" . mhtml-mode))

;; (defun vuejs-custom ()
;;   (setq vue-html-tab-width 2)
;;   (global-set-key (kbd "C-c C-l") 'vue-mode-reparse)
;;   (global-set-key (kbd "C-c C-e") 'vue-mode-edit-indirect-at-point)
;;   (add-to-list 'write-file-functions 'delete-trailing-whitespace)
;;   ;;(turn-on-diff-hl-mode)
;;   )

;; (add-hook 'vue-mode-hook 'vuejs-custom)

;; KEYS
(global-set-key [(control shift up)]  'move-line-up)
(global-set-key [(control shift down)]  'move-line-down)
(defun move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun move-line-down ()
  "Move down the current line."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

(setq scroll-error-top-bottom t)
(setq scroll-preserve-screen-position nil)
(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-x C-a") 'ibuffer)
;(global-set-key (kbd "C-'") 'ibuffer)
(global-set-key (kbd "M-z") 'zap-up-to-char)

;(global-set-key (kbd "C-s") 'isearch-forward-regexp)
;(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)

;; GIT
(use-package helm-ls-git
  :ensure t
  :init

  (global-set-key (kbd "C-S-p") 'helm-browse-project)
  (global-set-key (kbd "C-\"") 'helm-projects-history)
  )

(use-package projectile :ensure t)

(use-package helm
  :ensure t)

(use-package helm-projectile
  :ensure t)

(use-package helm-ag
  :ensure t
  :bind ("C-?" . helm-projectile-ag)
  :commands (helm-ag helm-projectile-ag)
  :init (
         setq helm-ag-insert-at-point 'symbol
              helm-ag-command-option "--path-to-ignore ~/.agignore"))

(setq projectile-mode-line "Projectile")

(setq remote-file-name-inhibit-cache nil)
(setq vc-ignore-dir-regexp
      (format "%s\\|%s"
                    vc-ignore-dir-regexp
                    tramp-file-name-regexp))
(setq tramp-verbose 1)
(use-package "magit"
  :ensure t)


(use-package flx :ensure t)
(use-package flx-ido :ensure t
  :init
;  (ido-mode 1)
;  (ido-everywhere 1)
  (flx-ido-mode 1)
  ;; disable ido faces to see flx highlights.
  (setq ido-enable-flex-matching t)
  (setq ido-use-faces nil)
  )

(use-package smex :ensure t)

(use-package counsel
  :ensure t)

(use-package ivy
  :ensure t
  :config (progn
            (ivy-mode 0)
            (setq ivy-use-virtual-buffers t)
            (setq enable-recursive-minibuffers t)
    (setq ivy-use-virtual-buffers t)
    (setq enable-recursive-minibuffers t)
    ;; enable this if you want `swiper' to use it
    ;; (setq search-default-mode #'char-fold-to-regexp)
    (global-set-key (kbd "C-c C-r") 'ivy-resume)
    (global-set-key (kbd "<f6>") 'ivy-resume)
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history) ;
))
(use-package hydra :ensure t)

(use-package ibuffer-vc
  :ensure t)

(define-globalized-minor-mode
  global-text-scale-mode
  text-scale-mode
  (lambda () (text-scale-mode 1)))

(defun global-text-scale-adjust (inc) (interactive)
       (text-scale-set 1)
       (kill-local-variable 'text-scale-mode-amount)
       (setq-default text-scale-mode-amount (+ text-scale-mode-amount inc))
       (global-text-scale-mode 1)
       )

(global-set-key (kbd "M-0")
                '(lambda () (interactive)
                   (global-text-scale-adjust (- text-scale-mode-amount))
                   (global-text-scale-mode -1)))
(global-set-key (kbd "M-+")
                '(lambda () (interactive) (global-text-scale-adjust 1)))
(global-set-key (kbd "M--")
                '(lambda () (interactive) (global-text-scale-adjust -1)))


(require 'markdown-mode)
(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(autoload 'gfm-mode "markdown-mode"
   "Major mode for editing GitHub Flavored Markdown files" t)
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))
(add-to-list 'auto-mode-alist '("readme\\.md\\'" . gfm-mode))

(setq tab-always-indent 'complete)
(setq-default indent-tabs-mode nil)

;; (use-package prettier :ensure t)
(use-package multiple-cursors
  :ensure t
  :config
  (global-set-key (kbd "C-c m c") 'mc/edit-lines)
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
  )
(require 'flycheck)
(add-hook 'after-init-hook #'global-flycheck-mode)

;; auto complete
(add-hook 'after-init-hook 'global-company-mode)
(setq company-tooltip-align-annotations t)

;; auto complete with docstrings
(company-quickhelp-mode)

(global-hl-line-mode 0)

(delete-selection-mode 1)

(emojify-set-emoji-styles '(unicode))
(add-hook 'after-init-hook #'global-emojify-mode)


(load "setup-clojure.el")

(use-package zig-mode :ensure t
  :config
  (add-to-list 'auto-mode-alist '("\\.zig\\'" . zig-mode)))


;; Use human readable Size column instead of original one
(define-ibuffer-column size-h
  (:name "Size" :inline t)
  (cond
   ((> (buffer-size) 1000000) (format "%7.1fM" (/ (buffer-size) 1000000.0)))
   ((> (buffer-size) 100000) (format "%7.0fk" (/ (buffer-size) 1000.0)))
   ((> (buffer-size) 1000) (format "%7.1fk" (/ (buffer-size) 1000.0)))
   (t (format "%8d" (buffer-size)))))

;; Modify the default ibuffer-formats
  (setq ibuffer-formats
	'((mark modified read-only " "
		(name 18 18 :left :elide)
		" "
		(size-h 9 -1 :right)
		" "
		(mode 16 16 :left :elide)
		" "
		filename-and-process)))
